<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.3" name="Base Tileset" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../../Assets/Art/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="Dirt" tile="578"/>
  <terrain name="Gravel" tile="0"/>
  <terrain name="Sand" tile="0"/>
  <terrain name="Grass1" tile="0"/>
  <terrain name="Grass2" tile="913"/>
  <terrain name="Grass3" tile="913"/>
  <terrain name="FlowerRed" tile="913"/>
  <terrain name="FlowerWhite" tile="913"/>
  <terrain name="FlowerBlue" tile="913"/>
  <terrain name="Water1" tile="57"/>
  <terrain name="Water2" tile="116"/>
 </terraintypes>
 <tile id="2" terrain=",,,9">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0.181818" y="0.181818" width="16" height="15.8182"/>
  </objectgroup>
 </tile>
 <tile id="3" terrain=",,9,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="-0.0625" width="15.9375" height="16"/>
  </objectgroup>
 </tile>
 <tile id="4" terrain=",,9,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="-0.0625" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="57" terrain="9,9,9,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.03125" y="0.125" width="16" height="15.875"/>
  </objectgroup>
 </tile>
 <tile id="58" terrain="9,9,,9"/>
 <tile id="59" terrain=",9,,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="-0.125" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="60" terrain="9,9,9,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.0625" width="15.875" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="61" terrain="9,,9,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0.0625" width="16" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="114" terrain="9,,9,9">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0.03125" y="-0.0625" width="15.9063" height="16.0313"/>
  </objectgroup>
 </tile>
 <tile id="115" terrain=",9,9,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.125" y="-0.0625" width="15.9063" height="16.0313"/>
  </objectgroup>
 </tile>
 <tile id="116" terrain=",9,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="0.125" width="16" height="15.75"/>
  </objectgroup>
 </tile>
 <tile id="117" terrain="9,9,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0.0625" width="15.9375" height="16"/>
  </objectgroup>
 </tile>
 <tile id="118" terrain="9,,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0625" y="0.0625" width="15.9375" height="16"/>
  </objectgroup>
 </tile>
 <tile id="171" terrain="10,10,10,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.0473093" y="0.0473093" width="16.3217" height="15.9905"/>
  </objectgroup>
 </tile>
 <tile id="172" terrain="10,10,,10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.141928" y="-0.0473093" width="15.8013" height="16.0378"/>
   <object id="2" x="-0.0625" y="0.03125" width="16.0625" height="15.875"/>
  </objectgroup>
 </tile>
 <tile id="173" terrain=",,,10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.266928" y="-0.0169279" width="15.9375" height="15.8125"/>
  </objectgroup>
 </tile>
 <tile id="174" terrain=",,10,10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="0" width="16" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="175" terrain=",,10,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.125" y="0.0625" width="15.8125" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="228" terrain="10,,10,10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.09375" y="0" width="15.875" height="15.9375"/>
  </objectgroup>
 </tile>
 <tile id="229" terrain=",10,10,10">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="0.03125" width="16.0313" height="16"/>
  </objectgroup>
 </tile>
 <tile id="230" terrain=",10,,10"/>
 <tile id="231" terrain="10,10,10,10"/>
 <tile id="232" terrain="10,,10,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0434783" y="-0.0434783" width="15.913" height="15.913"/>
  </objectgroup>
 </tile>
 <tile id="287" terrain=",10,,"/>
 <tile id="288" terrain="10,10,,">
  <objectgroup draworder="index" id="3">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="289" terrain="10,,,"/>
 <tile id="342" terrain="6,6,6,"/>
 <tile id="343" terrain="6,6,,6"/>
 <tile id="344" terrain=",,,6"/>
 <tile id="345" terrain=",,6,6"/>
 <tile id="346" terrain=",,6,"/>
 <tile id="399" terrain="6,,6,6"/>
 <tile id="400" terrain=",6,6,6"/>
 <tile id="401" terrain=",6,,6"/>
 <tile id="402" terrain="6,6,6,6"/>
 <tile id="403" terrain="6,,6,"/>
 <tile id="458" terrain=",6,,"/>
 <tile id="459" terrain="6,6,,"/>
 <tile id="460" terrain="6,,,"/>
 <tile id="513" terrain="7,7,7,"/>
 <tile id="514" terrain="7,7,,7"/>
 <tile id="515" terrain=",,,7"/>
 <tile id="516" terrain=",,7,7"/>
 <tile id="517" terrain=",,7,"/>
 <tile id="518" terrain="0,0,0,"/>
 <tile id="519" terrain="0,0,,0"/>
 <tile id="520" terrain=",,,0"/>
 <tile id="521" terrain=",,0,0"/>
 <tile id="522" terrain=",,0,"/>
 <tile id="570" terrain="7,,7,7"/>
 <tile id="571" terrain=",7,7,7"/>
 <tile id="572" terrain=",7,,7"/>
 <tile id="573" terrain="7,7,7,7"/>
 <tile id="574" terrain="7,,7,"/>
 <tile id="575" terrain="0,,0,0"/>
 <tile id="576" terrain=",0,0,0"/>
 <tile id="577" terrain=",0,,0"/>
 <tile id="578" terrain="0,0,0,0"/>
 <tile id="579" terrain="0,,0,"/>
 <tile id="629" terrain=",7,,"/>
 <tile id="630" terrain="7,7,,"/>
 <tile id="631" terrain="7,,,"/>
 <tile id="634" terrain=",0,,"/>
 <tile id="635" terrain="0,0,,"/>
 <tile id="636" terrain="0,,,"/>
 <tile id="643">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0625" y="0.125" width="16.0625" height="15.75"/>
  </objectgroup>
 </tile>
 <tile id="684" terrain="8,8,8,"/>
 <tile id="685" terrain="8,8,,8"/>
 <tile id="686" terrain=",,,8">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0" y="0.0434783" width="15.913" height="15.913"/>
  </objectgroup>
 </tile>
 <tile id="687" terrain=",,8,8">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0434783" y="0.0434783" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="688" terrain=",,8,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.0869565" y="-0.0869565" width="16.1304" height="16.1304"/>
  </objectgroup>
 </tile>
 <tile id="741" terrain="8,,8,8"/>
 <tile id="742" terrain=",8,8,8"/>
 <tile id="743" terrain=",8,,8"/>
 <tile id="744" terrain="8,8,8,8"/>
 <tile id="745" terrain="8,,8,"/>
 <tile id="800" terrain=",8,,"/>
 <tile id="801" terrain="8,8,,"/>
 <tile id="802" terrain="8,,,"/>
 <tile id="855" terrain="3,3,3,"/>
 <tile id="856" terrain="3,3,,3"/>
 <tile id="857" terrain=",,,3"/>
 <tile id="858" terrain=",,3,3"/>
 <tile id="859" terrain=",,3,"/>
 <tile id="860" terrain="1,1,1,"/>
 <tile id="861" terrain="1,1,,1"/>
 <tile id="862" terrain=",,,1"/>
 <tile id="863" terrain=",,1,1"/>
 <tile id="864" terrain=",,1,"/>
 <tile id="912" terrain="3,,3,3"/>
 <tile id="913" terrain=",3,3,3"/>
 <tile id="914" terrain=",3,,3"/>
 <tile id="915" terrain="3,3,3,3"/>
 <tile id="916" terrain="3,,3,"/>
 <tile id="917" terrain="1,,1,1"/>
 <tile id="918" terrain=",1,1,1"/>
 <tile id="919" terrain=",1,,1"/>
 <tile id="920" terrain="1,1,1,1"/>
 <tile id="921" terrain="1,,1,"/>
 <tile id="971" terrain=",3,,"/>
 <tile id="972" terrain="3,3,,"/>
 <tile id="973" terrain="3,,,"/>
 <tile id="976" terrain=",1,,"/>
 <tile id="977" terrain="1,1,,"/>
 <tile id="978" terrain="1,,,"/>
 <tile id="1026" terrain="4,4,4,"/>
 <tile id="1027" terrain="4,4,,4"/>
 <tile id="1028" terrain=",,,4"/>
 <tile id="1029" terrain=",,4,4"/>
 <tile id="1030" terrain=",,4,"/>
 <tile id="1083" terrain="4,,4,4"/>
 <tile id="1084" terrain=",4,4,4"/>
 <tile id="1085" terrain=",4,,4"/>
 <tile id="1086" terrain="4,4,4,4"/>
 <tile id="1087" terrain="4,,4,"/>
 <tile id="1142" terrain=",4,,"/>
 <tile id="1143" terrain="4,4,,"/>
 <tile id="1144" terrain="4,,,"/>
 <tile id="1197" terrain="5,5,5,"/>
 <tile id="1198" terrain="5,5,,5"/>
 <tile id="1199" terrain=",,,5"/>
 <tile id="1200" terrain=",,5,5"/>
 <tile id="1201" terrain=",,5,"/>
 <tile id="1202" terrain="2,2,2,"/>
 <tile id="1203" terrain="2,2,,2"/>
 <tile id="1204" terrain=",,,2"/>
 <tile id="1205" terrain=",,2,2"/>
 <tile id="1206" terrain=",,2,"/>
 <tile id="1254" terrain="5,,5,5"/>
 <tile id="1255" terrain=",5,5,5"/>
 <tile id="1256" terrain=",5,,5"/>
 <tile id="1257" terrain="5,5,5,5"/>
 <tile id="1258" terrain="5,,5,"/>
 <tile id="1259" terrain="2,,2,2"/>
 <tile id="1260" terrain=",2,2,2"/>
 <tile id="1261" terrain=",2,,2"/>
 <tile id="1262" terrain="2,2,2,2"/>
 <tile id="1263" terrain="2,,2,"/>
 <tile id="1313" terrain=",5,,"/>
 <tile id="1314" terrain="5,5,,"/>
 <tile id="1315" terrain="5,,,"/>
 <tile id="1318" terrain=",2,,"/>
 <tile id="1319" terrain="2,2,,"/>
 <tile id="1320" terrain="2,,,"/>
</tileset>
